"""Módulo que funciona como programa principal.
Cambio con respecto al anterior: puede tener 1, 0 o varios procesadores"""

import sys

import sources
import sinks
import processes


def parse_args():

    if len(sys.argv) < 6:
        print(f'Usage: {sys.argv[0]} <source> <source_args> <process> <process_args> <sink> <sink_args> <duration>')
        exit(0)

    source = sys.argv[1]
    source_args = sys.argv[2]
    sink = sys.argv[len(sys.argv)-3]
    sink_args = sys.argv[len(sys.argv)-2]
    duration = float(sys.argv[len(sys.argv)-1])
    return source, source_args, sink, sink_args, duration


def main():
    source, source_args, sink, sink_args, dur = parse_args()

    if source == 'sin':
        sound = sources.sin(duration=dur, freq=int(source_args))
    elif source == 'constant':
        sound = sources.constant(duration=dur, positive=bool(source_args))
    elif source == 'square':
        sound = sources.square(duration=dur, freq=int(source_args))
    else:
        sys.exit("Unknown source")

    p=0
    for i in range(3, len(sys.argv)-3-sys.argv.count('add'), 2):

        process = sys.argv[i+p]

        if process != 'add':
            process_args = sys.argv[i+1+p]

            if process == 'ampli':
                processes.ampli(sound=sound, factor=float(process_args))
            elif process == 'reduce':
                processes.reduce(sound=sound, factor=int(process_args))
            elif process == 'extend':
                processes.extend(sound=sound, factor=int(process_args))

            else:
                sys.exit("Unknown process")

        elif process == 'add':

            source = sys.argv[i+1+p]
            source_args = sys.argv[i+2+p]
            sound = processes.add(sound=sound, dur=dur, source=source, source_args=source_args)
            p += 1

    if sink == 'play':
        sinks.play(sound)
    elif sink == 'draw':
        sinks.draw(sound=sound, period=float(sink_args), n_square=source)
    elif sink == 'store':
        sinks.store(sound=sound, path=str(sink_args))
    else:
        sys.exit("Unknown sink")


if __name__ == '__main__':
    main()
