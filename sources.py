"""Methods for producing sound (sound sources)"""

import array
import math
import config


def _samples(duration):
    """Compute the number of samples for a certain duration (sec)"""

    # Total number of samples (samples per second by number of seconds)
    return int(config.samples_second * duration)


def sin(duration: float, freq: float):
    """Produce a list of samples of a sinusoidal signal, of duratioon (sec)"""

    nsamples = _samples(duration)

    # Create an array of integers (signed shorts: 16 bit signed integers)
    sound = array.array('h', [0] * nsamples)

    # Compute sin samples (maximum amplitude, freq frequency)
    for nsample in range(nsamples):
        t = nsample / config.samples_second
        sound[nsample] = int(config.max_amp *
                             math.sin(2 * config.pi * freq * t))

    return sound


def constant(duration: float, positive: bool):
    """Produce a list of samples of a constant signal, of duration (sec)"""

    nsamples = _samples(duration)

    # Create an array of integers with either max or min amplitude
    if positive:
        sample = config.max_amp
    else:
        sample = - config.max_amp

    sound = array.array('h', [sample] * nsamples)
    return sound


"""Método que devuelve una señal cuadrada durante un tiempo de maxima amplitud"""


def square(duration: float, freq: float):

    nsamples = _samples(duration)
    sound = array.array('h', [0] * nsamples)
    n1 = 1

    for nsample in range(nsamples):
        if nsample % freq == 0:
            n1 *= -1
        sound[nsample] = (config.max_amp * n1)

    return sound
