# Maria Victoria Gago Riveros

Requisitos obligatorios:

- Método square
- Método store
- Método ampli
- Método reduce
- Método extend
- Extension de source_sink.py
- Modulo source_process_sink.py

Requisitos opcionales:

- Modulo sound_process.py
- Modulo sound_processes.py
- Función add
