"""Módulo que amplifica, reduce o expande el sonido dado por una muestra"""

import sys

import config
import sources


def ampli(sound, factor: float):

    for nsample in range(len(sound)):
        if sound[nsample] * factor >= config.max_amp:
            sound[nsample] = config.max_amp

        elif sound[nsample] * factor <= -config.max_amp:
            sound[nsample] = -config.max_amp

        elif sound[nsample] * factor < config.max_amp and sound[nsample] * factor > -config.max_amp:
            sound[nsample] = int(sound[nsample] * factor)

    return sound


def reduce(sound, factor: int):

    if factor > 0:
        del sound[factor-1::factor]

        return sound
    else:
        print('ERROR')


def extend(sound, factor: int):

    nsamples = len(sound)
    n = 0

    if factor > 0:
        for i in range(factor, nsamples, factor):
            if i > factor:
                n += 1
                i = i+n
                sound.insert(i, (sound[i - 1] + sound[i]) // 2)
            else:
                sound.insert(i, (sound[i - 1] + sound[i]) // 2)
        return sound
    else:
        print('ERROR: debe ser mayor que 0')


"""Método add que suma lo recibido en la etapa anterior y una fuente de señal (argumento)
Señal sinusoidal usada en la variable: sound2"""


def add(sound, dur: float, source, source_args):

    if source == 'sin':
        sound2 = sources.sin(duration=dur, freq=int(source_args))
    elif source == 'constant':
        sound2 = sources.constant(duration=dur, positive=bool(source_args))
    elif source == 'square':
        sound2 = sources.square(duration=dur, freq=int(source_args))
    else:
        sys.exit("Unknown source")

    return sound + sound2



