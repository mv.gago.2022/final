"""Módulo que trabaja como programa principal. Ejecuta una fuente, un procesador y un sumidero de sonido"""

import sys

import processes
import sources
import sinks


def parse_args():

    if len(sys.argv) != 5:
        print(f"Usage: {sys.argv[0]} <source> <process> <sink> <duration> ")
        exit(0)
    source = sys.argv[1]
    process = sys.argv[2]
    sink = sys.argv[3]
    duration = float(sys.argv[4])

    return source, process, sink, duration


def main():
    source, process, sink, dur = parse_args()

    if source == 'sin':
        sound = sources.sin(duration=dur, freq=400)
    elif source == 'constant':
        sound = sources.constant(duration=dur, positive=True)
    elif source == 'square':
        sound = sources.square(duration=dur, freq=400)
    else:
        sys.exit("Unknown source")

    if process == 'ampli':
        processes.ampli(sound=sound, factor=4)
    elif process == 'reduce':
        processes.reduce(sound=sound, factor=4)
    elif process == 'extend':
        processes.extend(sound=sound, factor=4)
    else:
        sys.exit("Unknown process")

    if sink == 'play':
        sinks.play(sound)
    elif sink == 'draw':
        sinks.draw(sound=sound, period=0.0001, n_square=sound)
    elif sink == 'store':
        sinks.store(sound=sound, path='stored.wav')
    else:
        sys.exit("Unknown sink")


if __name__ == '__main__':
    main()
